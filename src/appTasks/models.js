const mongoose = require("mongoose");
const dotenv = require("dotenv");
dotenv.config();
const mongo_db_url = process.env.MONGO_DB_URL;

mongoose
  .connect(`${mongo_db_url}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log("MongoDB connected"))
  .catch((error) => console.error("MongoDB connection error", error));

const TaskSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: false,
  },
  tags: {
    type: [String],
    required: false,
    default: [],
  },
  done: {
    type: Boolean,
    required: true,
    default: false,
  },
});

const TaskModel = mongoose.model("Task", TaskSchema);

module.exports = { TaskModel };
