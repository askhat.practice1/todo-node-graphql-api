const { TaskModel } = require("./models");

const resolvers = {
  Query: {
    getTodoTasks: () => TaskModel.find({ done: false }),
    getDoneTasks: () => TaskModel.find({ done: true }),
    getTask: (root, { id }) => TaskModel.findById(id),
    findTasks: (root, { tags }) => TaskModel.find({ tags: { $in: tags } }),
  },
  Mutation: {
    createTask: (root, { task }) => {
      const newTask = new TaskModel(task);
      return newTask.save();
    },
    updateTask: async (root, { id, task }) => {
      await TaskModel.findByIdAndUpdate(id, task);
      return TaskModel.findById(id);
    },
    deleteTask: async (root, { id }) => {
      const deletedTask = await TaskModel.findByIdAndDelete(id);
      return deletedTask ? true : false;
    },
  },
};

module.exports = resolvers;
