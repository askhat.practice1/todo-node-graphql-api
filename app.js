const express = require("express");
const { graphqlHTTP } = require("express-graphql");
const { schema } = require("./src/appTasks/schema");

const app = express();

app.get("/", (req, res) => {
  res.send("Graphql is amazing!");
});

app.use(
  "/graphql",
  graphqlHTTP({
    schema: schema,
    graphiql: true,
  })
);

// Start the server
const port = process.env.PORT || 3000;
const host = process.env.HOST || "0.0.0.0";
app.listen(port, host, () => {
  console.log(`Server listening on http://${host}:${port}/graphql`);
});
